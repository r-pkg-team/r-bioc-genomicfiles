r-bioc-genomicfiles (1.42.0+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Tue, 14 Jan 2025 08:44:54 +0100

r-bioc-genomicfiles (1.42.0+dfsg-1) experimental; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)

 -- Michael R. Crusoe <crusoe@debian.org>  Sat, 30 Nov 2024 23:07:29 +0100

r-bioc-genomicfiles (1.40.0+dfsg-1) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Sun, 18 Aug 2024 12:36:11 +0200

r-bioc-genomicfiles (1.40.0+dfsg-1~0exp1) experimental; urgency=medium

  * Team upload.
  * d/{,tests/}control: bump minimum versions of r-bioc-* packages to
    bioc-3.19+ versions

 -- Michael R. Crusoe <crusoe@debian.org>  Wed, 07 Aug 2024 16:29:33 +0200

r-bioc-genomicfiles (1.40.0+dfsg-1~0exp) experimental; urgency=low

  * Team upload.
  * New upstream version
  * Set upstream metadata fields: Archive.
  * d/tests/autopkgtest-pkg-r.conf: skip on 32-bit architectures.

 -- Michael R. Crusoe <crusoe@debian.org>  Sat, 13 Jul 2024 16:59:34 +0200

r-bioc-genomicfiles (1.38.0+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Fixed debian/watch for BioConductor

 -- Andreas Tille <tille@debian.org>  Fri, 01 Dec 2023 11:31:21 +0100

r-bioc-genomicfiles (1.36.0+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * Remove autogenerated HTML docs since it is including compressed JS

 -- Andreas Tille <tille@debian.org>  Thu, 27 Jul 2023 21:46:52 +0200

r-bioc-genomicfiles (1.34.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Reduce piuparts noise in transitions (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 22 Nov 2022 07:24:39 +0100

r-bioc-genomicfiles (1.32.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 17 May 2022 07:41:05 +0200

r-bioc-genomicfiles (1.30.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 26 Nov 2021 13:54:51 +0100

r-bioc-genomicfiles (1.28.0-2) unstable; urgency=medium

  * Team upload.
  * Provide autopkgtest-pkg-r.conf to make sure RUnit will be found
  * Drop debian/tests/control and rely on autopkgtest-pkg-r
  * Disable reprotest

 -- Andreas Tille <tille@debian.org>  Tue, 14 Sep 2021 12:43:03 +0200

r-bioc-genomicfiles (1.28.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * dh-update-R to update Build-Depends (3) (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 14 Sep 2021 10:30:15 +0200

r-bioc-genomicfiles (1.26.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Dylan Aïssi <daissi@debian.org>  Wed, 18 Nov 2020 10:57:11 +0100

r-bioc-genomicfiles (1.24.0-2) unstable; urgency=medium

  * Team upload.
  * debhelper-compat 13 (routine-update)
  * Trim trailing whitespace.

 -- Andreas Tille <tille@debian.org>  Thu, 17 Sep 2020 14:10:07 +0200

r-bioc-genomicfiles (1.24.0-1) unstable; urgency=medium

  * Initial release (closes: #969061)

 -- Steffen Moeller <moeller@debian.org>  Wed, 26 Aug 2020 23:48:15 +0200
